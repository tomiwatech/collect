## Collect

Collect makes it easier to accept submissions from different users with metadata and integrations

---

## Setup

    $ git clone https://gitlab.com/tomiwatech/collect.git/
    $ cd collect

## Configure app

check the `config.yml` file, modify it and add the correct values.

## Google Sheet
Generate credentials from google console for google sheet integration. Save it at the root of the project with `credentials.json`
You can check how to create a project and enable the Google Sheet API [here](https://developers.google.com/workspace/guides/create-project)


## Running the project 
To run the project locally, execute the command below

```make build_and_serve```

## Integration Tests
To run integration tests run 

```make integration_tests```

## Acceptance Tests
To run acceptance/end to end tests run

```make acceptance_tests```

This will start the database and application

## Deployment

The application is deployed on [https://atlan.softsignatureslab.com](https://atlan.softsignatureslab.com/)

## Documentation

[Postman Collection](https://documenter.getpostman.com/view/3064040/VUjSGisx)

## Sample Sheet

![img_2.png](img_2.png)