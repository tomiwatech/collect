package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/tomiwatech/collect/internal/database"
	"gitlab.com/tomiwatech/collect/internal/form"
	"net/http"
)

var CreateForm = func(w http.ResponseWriter, r *http.Request) {
	var userForm form.Form
	if err := json.NewDecoder(r.Body).Decode(&userForm); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	db := database.Connection()
	formService := form.NewForm(&db)
	createForm, err := formService.CreateForm(context.Background(), form.Form{
		Name: userForm.Name,
	})

	if err != nil {
		fmt.Println(err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	response := Message{
		Code:    http.StatusCreated,
		Message: "Form created successfully",
		Status:  "success",
		Data:    createForm,
	}

	respondWithJson(w, http.StatusCreated, response)
}

var GetForm = func(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	name := params["name"]

	fmt.Println(name)

	if len(name) == 0 {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload, please supply name")
		return
	}

	db := database.Connection()
	formService := form.NewForm(&db)
	formResponse, err := formService.GetForm(context.Background(), name)

	if err != nil {
		fmt.Println(err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	response := Message{
		Code:    http.StatusOK,
		Message: "Successfully fetched form",
		Status:  "success",
		Data:    formResponse,
	}

	respondWithJson(w, http.StatusOK, response)
}

var GetForms = func(w http.ResponseWriter, r *http.Request) {

	db := database.Connection()
	formService := form.NewForm(&db)
	forms, err := formService.GetForms(context.Background())
	if err != nil {
		fmt.Println(err)
	}

	response := Message{
		Code:    http.StatusOK,
		Message: "Successfully fetched forms",
		Status:  "success",
		Data:    forms,
	}
	respondWithJson(w, http.StatusOK, response)
}

func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJson(w, code, map[string]string{"error": msg})
}

func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, err := w.Write(response)
	if err != nil {
		return
	}
}
