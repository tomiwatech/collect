package controller

import (
	"net/http"
)

type Message struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Status  string      `json:"status"`
	Data    interface{} `json:"data"`
}

var HealthCheck = func(w http.ResponseWriter, r *http.Request) {

	responseMessage := Message{
		Code:    200,
		Message: "Welcome to Atlan Collect",
		Status:  "success",
		Data:    nil,
	}

	respondWithJson(w, http.StatusOK, responseMessage)
}

var ApiVersionIndex = func(w http.ResponseWriter, r *http.Request) {

	responseMessage := Message{
		Code:    200,
		Message: "Welcome to API Version 1",
		Status:  "success",
		Data:    nil,
	}

	respondWithJson(w, http.StatusOK, responseMessage)
}
