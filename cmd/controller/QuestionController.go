package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/tomiwatech/collect/internal/database"
	"gitlab.com/tomiwatech/collect/internal/integration"
	"gitlab.com/tomiwatech/collect/internal/question"
	"net/http"
)

var CreateQuestion = func(w http.ResponseWriter, r *http.Request) {
	var userQuestion question.Question
	if err := json.NewDecoder(r.Body).Decode(&userQuestion); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	db := database.Connection()
	questionService := question.NewQuestion(&db)
	questionResponse, err := questionService.CreateQuestion(context.Background(), userQuestion)
	if err != nil {
		fmt.Println(err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	questions := make([]string, 0)
	for _, item := range userQuestion.QuestionItem {
		questions = append(questions, item.Question)
	}

	integration.WriteToSheet(questions) // write the questions to sheet

	response := Message{
		Code:    http.StatusCreated,
		Message: "Questions created successfully",
		Status:  "success",
		Data:    questionResponse,
	}

	respondWithJson(w, http.StatusCreated, response)
}

var GetQuestion = func(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	formId := params["formId"]

	fmt.Println(formId)

	if len(formId) == 0 {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload, please supply formId")
		return
	}

	db := database.Connection()
	questionService := question.NewQuestion(&db)
	questionResponse, err := questionService.GetQuestion(context.Background(), formId)
	if err != nil {
		fmt.Println(err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	response := Message{
		Code:    http.StatusCreated,
		Message: "Successfully fetched question",
		Status:  "success",
		Data:    questionResponse,
	}

	respondWithJson(w, http.StatusOK, response)
}

var GetQuestions = func(w http.ResponseWriter, r *http.Request) {

	db := database.Connection()
	questionService := question.NewQuestion(&db)
	questionResponse, err := questionService.GetQuestions(context.Background())
	if err != nil {
		fmt.Println(err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	response := Message{
		Code:    http.StatusOK,
		Message: "Fetched all questions",
		Status:  "success",
		Data:    questionResponse,
	}

	respondWithJson(w, http.StatusOK, response)
}
