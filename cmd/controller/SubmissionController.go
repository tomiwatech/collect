package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/tomiwatech/collect/internal/database"
	"gitlab.com/tomiwatech/collect/internal/rabbitmq"
	"gitlab.com/tomiwatech/collect/internal/submission"
	"net/http"
)

var CreateSubmission = func(w http.ResponseWriter, r *http.Request) {
	var userSubmission submission.Submission
	if err := json.NewDecoder(r.Body).Decode(&userSubmission); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	db := database.Connection()
	submissionService := submission.NewSubmission(&db)
	_, submissionId, err := submissionService.SubmissionStore.CreateSubmission(context.Background(), userSubmission)
	if err != nil {
		fmt.Println(err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	rabbitmq.PublishMessageToQueue(userSubmission, submissionId)
	respondWithJson(w, http.StatusCreated, "Submission successfully submitted")
}

var GetSubmission = func(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	formId := params["formId"]

	fmt.Println(formId)

	if len(formId) == 0 {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload, please supply formId")
		return
	}

	db := database.Connection()
	submissionService := submission.NewSubmission(&db)
	submissionResponse, err := submissionService.SubmissionStore.GetSubmission(context.Background(), formId)
	if err != nil {
		fmt.Println(err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJson(w, http.StatusOK, submissionResponse)
}

var GetSubmissions = func(w http.ResponseWriter, r *http.Request) {
	db := database.Connection()
	submissionService := submission.NewSubmission(&db)
	submissionResponse, err := submissionService.SubmissionStore.GetSubmissions(context.Background())
	if err != nil {
		fmt.Println(err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJson(w, http.StatusOK, submissionResponse)
}
