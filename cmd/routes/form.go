package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/tomiwatech/collect/cmd/controller"
)

func FormRouter(r *mux.Router) *mux.Router {
	s := r.PathPrefix("/forms").Subrouter()
	s.HandleFunc("", controller.CreateForm).Methods("POST")
	s.HandleFunc("", controller.GetForms).Methods("GET")
	s.HandleFunc("/{name}", controller.GetForm).Methods("GET")
	return s
}
