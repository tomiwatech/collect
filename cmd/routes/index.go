package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/tomiwatech/collect/cmd/controller"
)

func IndexRouter(r *mux.Router) *mux.Router {
	r.HandleFunc("/", controller.HealthCheck).Methods("GET")
	r.HandleFunc("/api", controller.ApiVersionIndex).Methods("GET")
	return r
}
