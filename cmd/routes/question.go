package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/tomiwatech/collect/cmd/controller"
)

func QuestionRouter(r *mux.Router) *mux.Router {
	s := r.PathPrefix("/questions").Subrouter()
	s.HandleFunc("", controller.CreateQuestion).Methods("POST")
	s.HandleFunc("/", controller.GetQuestions).Methods("GET")
	s.HandleFunc("/{formId}", controller.GetQuestion).Methods("GET")
	return s
}
