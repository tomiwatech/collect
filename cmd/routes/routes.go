package routes

import (
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func Routers() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	IndexRouter(router)
	subRouter := router.PathPrefix("/api").Subrouter()
	FormRouter(subRouter)
	QuestionRouter(subRouter)
	SubmissionRouter(subRouter)
	router.Use(loggingMiddleware)
	return router
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Info(r.RequestURI)
		next.ServeHTTP(w, r)
	})
}
