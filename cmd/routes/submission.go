package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/tomiwatech/collect/cmd/controller"
)

func SubmissionRouter(r *mux.Router) *mux.Router {
	s := r.PathPrefix("/submissions").Subrouter()
	s.HandleFunc("", controller.CreateSubmission).Methods("POST")
	s.HandleFunc("", controller.GetSubmissions).Methods("GET")
	s.HandleFunc("/{formId}", controller.GetSubmission).Methods("GET")
	return s
}
