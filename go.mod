module gitlab.com/tomiwatech/collect

go 1.16

require (
	cloud.google.com/go/compute v1.8.0 // indirect
	github.com/go-resty/resty/v2 v2.7.0 // indirect
	github.com/google/uuid v1.3.0
	github.com/googleapis/gax-go/v2 v2.5.1 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.5
	github.com/sirupsen/logrus v1.9.0
	github.com/spf13/viper v1.12.0
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.8.0
	go.mongodb.org/mongo-driver v1.10.1
	golang.org/x/net v0.0.0-20220811182439-13a9a731de15
	golang.org/x/oauth2 v0.0.0-20220808172628-8227340efae7
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
	google.golang.org/api v0.92.0
	google.golang.org/genproto v0.0.0-20220810155839-1856144b1d9c // indirect
)
