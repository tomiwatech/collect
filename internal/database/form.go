package database

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
	"gitlab.com/tomiwatech/collect/internal/form"
	"go.mongodb.org/mongo-driver/bson"
	"log"
)

func (d *Database) GetForm(
	ctx context.Context,
	name string,
) (form.FormDTO, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.FormCollection)
	var dbForm form.FormDTO
	filter := bson.D{{"name", name}}
	err := collection.FindOne(ctx, filter).Decode(&dbForm)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Found a single document: %+v\n", dbForm)

	return dbForm, nil
}

func (d *Database) GetForms(ctx context.Context) ([]form.FormDTO, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.FormCollection)
	filter := bson.D{}
	cursor, err := collection.Find(ctx, filter)
	if err != nil {
		panic(err)
	}

	var allForms []form.FormDTO
	if err = cursor.All(ctx, &allForms); err != nil {
		panic(err)
	}

	return allForms, nil
}

func (d *Database) CreateForm(ctx context.Context, userForm form.Form) (form.Form, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.FormCollection)
	toFormDTO := form.ToFormDTO(userForm)
	toFormDTO.ID = uuid.New().String()
	insertResult, err := collection.InsertOne(ctx, toFormDTO)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Inserted a single document: ", insertResult.InsertedID)

	var dbForm form.FormDTO
	filter := bson.D{{"_id", insertResult.InsertedID}}
	err = collection.FindOne(ctx, filter).Decode(&dbForm)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Found a single document: %+v\n", dbForm)
	formModel := form.ToFormModel(dbForm)

	return formModel, nil
}
