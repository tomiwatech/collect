// +build integration

package database

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tomiwatech/collect/internal/form"
	"testing"
)

func TestFormDatabase(t *testing.T) {
	t.Run("should create a form", func(t *testing.T) {
		db, err := NewDatabase()
		assert.NoError(t, err)

		formResponse, err := db.CreateForm(context.Background(), form.Form{
			Name: "Test Form",
		})
		assert.NoError(t, err)
		assert.Equal(t, "Test Form", formResponse.Name)
	})

	t.Run("should get a form", func(t *testing.T) {
		db, err := NewDatabase()
		assert.NoError(t, err)

		formResponse, err := db.CreateForm(context.Background(), form.Form{
			Name: "Get Form",
		})
		assert.NoError(t, err)

		newForm, err := db.GetForm(context.Background(), formResponse.Name)
		assert.NoError(t, err)
		assert.Equal(t, "Get Form", newForm.Name)
	})

	t.Run("should get all forms", func(t *testing.T) {
		db, err := NewDatabase()
		assert.NoError(t, err)

		newForms, err := db.GetForms(context.Background())
		assert.NoError(t, err)
		assert.Greater(t, len(newForms), 0)
	})
}
