package database

import (
	"context"
	"fmt"
	"gitlab.com/tomiwatech/collect/internal/integration"
	"go.mongodb.org/mongo-driver/bson"
	"log"
)

func (d *Database) GoogleSheet(ctx context.Context, integrationData integration.Data) (integration.Data, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.IntegrationDataCollection)
	insertResult, err := collection.InsertOne(ctx, integrationData)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Inserted a single integrationData: ", insertResult.InsertedID)

	var integrationDataResponse integration.Data
	filter := bson.D{{Key: "_id", Value: insertResult.InsertedID}}
	err = collection.FindOne(ctx, filter).Decode(&integrationDataResponse)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Found a single integration: %+v\n", integrationDataResponse)

	return integrationDataResponse, nil
}

func (d *Database) SearchSlang(ctx context.Context, integrationData integration.Data) (integration.Data, error) {
	return integration.Data{}, integration.ErrorNotImplemented
}

func (d *Database) SendSMS(ctx context.Context, integrationData integration.Data) (integration.Data, error) {
	return integration.Data{}, integration.ErrorNotImplemented
}
