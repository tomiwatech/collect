// +build integration

package database

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tomiwatech/collect/internal/integration"
)

func TestIntegrationDatabase(t *testing.T) {
	t.Run("should create a new integration", func(t *testing.T) {
		db, err := NewDatabase()
		assert.NoError(t, err)

		integrationResponse, err := db.GoogleSheet(context.Background(), integration.Data{
			Name:         "Test Integration",
			SubmissionId: "ab00cd3f-9bbf-4584-9376-676876585a71",
			FormId:       "kb00cd3f-9bbf-4584-9376-676876585a72",
		})
		assert.NoError(t, err)
		assert.Equal(t, "Test Integration", integrationResponse.Name)
	})
}
