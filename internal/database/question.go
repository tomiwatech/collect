package database

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tomiwatech/collect/internal/question"
	"go.mongodb.org/mongo-driver/bson"
)

func (d *Database) GetQuestion(
	ctx context.Context,
	formId string,
) (question.ItemDTO, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.QuestionCollection)
	var userQuestion question.ItemDTO
	filter := bson.D{{Key: "formId", Value: formId}}
	err := collection.FindOne(ctx, filter).Decode(&userQuestion)
	if err != nil {
		fmt.Println(err)
		return question.ItemDTO{}, question.ErrorFetchingQuestion
	}

	fmt.Printf("Found a single question: %+v\n", userQuestion)

	return userQuestion, nil
}

func (d *Database) GetQuestionsByForm(
	ctx context.Context,
	formId string,
) ([]question.ItemDTO, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.QuestionCollection)
	filter := bson.D{{Key: "formId", Value: formId}}
	questionCursor, err := collection.Find(ctx, filter)
	if err != nil {
		log.Fatal(err)
	}
	var questions []question.ItemDTO
	if err = questionCursor.All(ctx, &questions); err != nil {
		log.Fatal(err)
	}
	fmt.Println(questions)

	return questions, nil
}

func (d *Database) GetQuestions(
	ctx context.Context,
) ([]question.ItemDTO, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.QuestionCollection)
	filter := bson.D{}
	questionCursor, err := collection.Find(ctx, filter)
	if err != nil {
		log.Fatal(err)
	}
	var questions []question.ItemDTO
	if err = questionCursor.All(ctx, &questions); err != nil {
		log.Fatal(err)
	}
	fmt.Println(questions)

	return questions, nil
}

func (d *Database) CreateQuestion(ctx context.Context, userQuestion question.Question) (question.Question, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.QuestionCollection)
	questionDTO := question.ToQuestionDTO(userQuestion)

	allQuestions := make([]interface{}, 0)
	for _, item := range questionDTO {
		currentQuestionDTO := question.ItemDTO{
			ID:                  uuid.New().String(),
			Question:            item.Question,
			QuestionType:        item.QuestionType,
			Description:         item.Description,
			FormId:              item.FormId,
			Keyword:             item.Keyword,
			ImageUrl:            item.ImageUrl,
			MaxCharacterAllowed: item.MaxCharacterAllowed,
		}
		allQuestions = append(allQuestions, currentQuestionDTO)
	}

	_, err := collection.InsertMany(ctx, allQuestions)
	if err != nil {
		log.Fatal(err)
	}

	return question.Question{}, nil
}
