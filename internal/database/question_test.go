// +build integration

package database

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tomiwatech/collect/internal/question"
	"testing"
)

func TestQuestionDatabase(t *testing.T) {
	t.Run("should create a question", func(t *testing.T) {
		db, err := NewDatabase()
		assert.NoError(t, err)

		sampleQuestions := make([]question.Item, 0)
		sampleQuestions = append(sampleQuestions, question.Item{
			Question:            "Question One",
			Description:         "Testing",
			QuestionType:        "Text",
			Keyword:             "work",
			ImageUrl:            "google.com",
			MaxCharacterAllowed: 10,
		})
		_, err = db.CreateQuestion(context.Background(), question.Question{
			FormId:       "ab00cd3f-9bbf-4584-9376-676876585a71",
			QuestionItem: sampleQuestions,
		})

		assert.NoError(t, err)
	})

	t.Run("should get a question", func(t *testing.T) {
		db, err := NewDatabase()
		assert.NoError(t, err)

		questions, err := db.GetQuestions(context.Background())
		assert.NoError(t, err)

		quest, err := db.GetQuestion(context.Background(), questions[0].FormId)
		assert.NoError(t, err)
		assert.Equal(t, "Question One", quest.Question)
		assert.Equal(t, "Testing", quest.Description)
		assert.Equal(t, "Text", quest.QuestionType)
		assert.Equal(t, "ab00cd3f-9bbf-4584-9376-676876585a71", quest.FormId)
		assert.Equal(t, "work", quest.Keyword)
		assert.Equal(t, "google.com", quest.ImageUrl)
		assert.Equal(t, 10, quest.MaxCharacterAllowed)
	})

	t.Run("should get all questions", func(t *testing.T) {
		db, err := NewDatabase()
		assert.NoError(t, err)
		questions, err := db.GetQuestions(context.Background())
		assert.NoError(t, err)
		assert.Greater(t, len(questions), 0)
	})
}
