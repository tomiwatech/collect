package database

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/tomiwatech/collect/internal/submission"
	"go.mongodb.org/mongo-driver/bson"
)

func (d *Database) GetSubmission(
	ctx context.Context,
	formId string,
) (submission.SubmissionData, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.SubmissionCollection)
	var submissionResponse submission.SubmissionData
	filter := bson.D{{Key: "formId", Value: formId}}
	err := collection.FindOne(ctx, filter).Decode(&submissionResponse)
	if err != nil {
		fmt.Println(err)
		return submission.SubmissionData{}, submission.ErrorFetchingSubmission
	}

	fmt.Printf("Found a single submission: %+v\n", submissionResponse)

	return submissionResponse, nil
}

func (d *Database) GetSubmissionsByForm(
	ctx context.Context,
	formId string,
) ([]submission.SubmissionData, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.SubmissionCollection)
	filter := bson.D{{Key: "formId", Value: formId}}
	submissionCursor, err := collection.Find(ctx, filter)
	if err != nil {
		log.Fatal(err)
	}
	var submissions []submission.SubmissionData
	if err = submissionCursor.All(ctx, &submissions); err != nil {
		log.Fatal(err)
	}
	fmt.Println(submissions)

	return submissions, nil
}

func (d *Database) GetSubmissions(
	ctx context.Context,
) ([]submission.SubmissionData, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.SubmissionCollection)
	filter := bson.D{}
	submissionCursor, err := collection.Find(ctx, filter)
	if err != nil {
		log.Fatal(err)
	}
	var submissions []submission.SubmissionData
	if err = submissionCursor.All(ctx, &submissions); err != nil {
		log.Fatal(err)
	}
	fmt.Println(submissions)

	return submissions, nil
}

func (d *Database) CreateSubmission(ctx context.Context, userSubmission submission.Submission) (submission.SubmissionData, string, error) {
	collection := d.Client.Database(configInfo.Database.Name).Collection(configInfo.Database.SubmissionCollection)
	submissionDTO := submission.ToSubmissionDTO(userSubmission)
	insertedSubmission, err := collection.InsertOne(ctx, submissionDTO)
	if err != nil {
		log.Fatal(err)
	}

	submissionId := fmt.Sprint(insertedSubmission.InsertedID)
	return submission.SubmissionData{}, submissionId, nil
}
