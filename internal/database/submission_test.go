// +build integration

package database

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tomiwatech/collect/internal/submission"
	"testing"
)

func TestSubmissionDatabase(t *testing.T) {
	t.Run("should create a submission", func(t *testing.T) {
		db, err := NewDatabase()
		assert.NoError(t, err)

		sampleSubmission := make([]submission.SubmissionItem, 0)
		sampleSubmission = append(sampleSubmission, submission.SubmissionItem{
			QuestionId: "kb00cd3f-9bbf-4584-9376-676876585a7d",
			Response:   "Hello there",
		})
		submissionResponse, _, err := db.CreateSubmission(context.Background(), submission.Submission{
			FormId:         "ab00cd3f-9bbf-4584-9376-676876585a71",
			SubmissionItem: sampleSubmission,
		})

		assert.NoError(t, err)
		assert.Equal(t, "ab00cd3f-9bbf-4584-9376-676876585a71", submissionResponse.FormId)
	})

	t.Run("should get all submissions", func(t *testing.T) {
		db, err := NewDatabase()
		assert.NoError(t, err)
		submissions, err := db.GetSubmissions(context.Background())
		assert.NoError(t, err)
		assert.Greater(t, len(submissions), 0)
	})
}
