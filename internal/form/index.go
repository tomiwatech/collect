package form

import (
	"context"
	"errors"
	"fmt"
)

var (
	ErrorFetchingForm = errors.New("failed to fetch form by id")
	ErrorCreatingForm = errors.New("failed to create a new form")
)

type Form struct {
	Name string `json:"name"`
}

type FormDTO struct {
	ID   string `bson:"_id"`
	Name string `bson:"name"`
}

type FormStore interface {
	GetForm(context.Context, string) (FormDTO, error)
	GetForms(context.Context) ([]FormDTO, error)
	CreateForm(context.Context, Form) (Form, error)
}

type Service struct {
	FormStore FormStore
}

// NewForm - Accept interfaces, return structs
func NewForm(formStore FormStore) *Service {
	return &Service{
		FormStore: formStore,
	}
}

func ToFormDTO(formModel Form) FormDTO {
	return FormDTO{
		Name: formModel.Name,
	}
}

func ToFormModel(formDTO FormDTO) Form {
	return Form{
		Name: formDTO.Name,
	}
}

func (s *Service) GetForm(ctx context.Context, formName string) (FormDTO, error) {
	fmt.Println("Retrieving a form")
	form, err := s.FormStore.GetForm(ctx, formName)
	if err != nil {
		fmt.Println(err)
		return FormDTO{}, ErrorFetchingForm
	}

	return form, nil
}

func (s *Service) GetForms(ctx context.Context) ([]FormDTO, error) {
	fmt.Println("Retrieving forms")
	forms, err := s.FormStore.GetForms(ctx)
	if err != nil {
		fmt.Println(err)
		return []FormDTO{}, ErrorFetchingForm
	}

	return forms, nil
}

func (s *Service) CreateForm(ctx context.Context, form Form) (Form, error) {
	fmt.Println("creating a form")
	form, err := s.FormStore.CreateForm(ctx, form)
	if err != nil {
		fmt.Println(err)
		return Form{}, ErrorCreatingForm
	}
	return form, nil
}
