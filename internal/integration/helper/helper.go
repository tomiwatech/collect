package helper

import (
	"context"
	"fmt"
	"gitlab.com/tomiwatech/collect/internal/database"
	"gitlab.com/tomiwatech/collect/internal/integration"
)

func ProcessGoogleSheetIntegration(integrationData integration.Data, values []string) {
	db, dbErr := database.NewDatabase()
	if dbErr != nil {
		fmt.Println("failed to connect to the database")
		fmt.Println(dbErr)
		return
	}

	integrationService := integration.NewIntegrationData(db)
	integrationDataResponse, err := integrationService.IntegrationStore.GoogleSheet(context.Background(), integrationData)
	if err != nil {
		fmt.Println(err)
		return
	}

	integration.WriteToSheet(values)

	fmt.Println(integrationDataResponse)
}
