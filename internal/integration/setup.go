package integration

import (
	"context"
	"errors"
	"fmt"
)

var (
	ErrorSettingUpIntegration = errors.New("error setting up integration")
	ErrorNotImplemented       = errors.New("not implemented")
)

type Data struct {
	Name         string `json:"name"`
	SubmissionId string `json:"submissionId"`
	FormId       string `json:"formId"`
}

type IntegrationStore interface {
	GoogleSheet(context.Context, Data) (Data, error)
	SendSMS(context.Context, Data) (Data, error)
	SearchSlang(context.Context, Data) (Data, error)
}

type Integration struct {
	IntegrationStore IntegrationStore
}

// NewIntegrationData - Accept interfaces, return structs
func NewIntegrationData(integrationStore IntegrationStore) *Integration {
	return &Integration{
		IntegrationStore: integrationStore,
	}
}

func (s *Integration) GoogleSheet(ctx context.Context, integrationData Data) (Data, error) {
	fmt.Println("Retrieving an integration")
	booking, err := s.IntegrationStore.GoogleSheet(ctx, integrationData)
	if err != nil {
		fmt.Println(err)
		return Data{}, ErrorSettingUpIntegration
	}

	return booking, nil
}

func (s *Integration) SendSMS(ctx context.Context, integration Data) error {
	return ErrorNotImplemented
}

func (s *Integration) SearchSlang(ctx context.Context, integration Data) error {
	return ErrorNotImplemented
}
