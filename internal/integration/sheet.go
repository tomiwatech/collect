package integration

import (
	viperConfig "gitlab.com/tomiwatech/collect/internal/config"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
	"google.golang.org/api/sheets/v4"
	"io/ioutil"
	"log"
)

var configInfo viperConfig.Config

func init() {
	configInfo.Read()
}

func WriteToSheet(values []string) {
	ctx := context.Background()
	b, err := ioutil.ReadFile("../../credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	config, err := google.JWTConfigFromJSON(b, "https://www.googleapis.com/auth/spreadsheets")
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	client := config.Client(oauth2.NoContext)

	srv, err := sheets.NewService(ctx, option.WithHTTPClient(client))
	if err != nil {
		log.Fatalf("Unable to retrieve Sheets Client %v", err)
	}

	spreadsheetId := configInfo.Server.SheetId
	writeRange := "A1"

	var vr sheets.ValueRange

	convertedValues := convertStringArrayToInterface(values)
	vr.Values = append(vr.Values, convertedValues)

	_, err = srv.Spreadsheets.Values.Append(spreadsheetId, writeRange, &vr).ValueInputOption("RAW").Do()
	if err != nil {
		log.Fatalf("Unable to retrieve data from sheet. %v", err)
	}

}

func convertStringArrayToInterface(data []string) []interface{} {
	slice := make([]interface{}, len(data))
	for index, item := range data {
		slice[index] = item
	}
	return slice
}
