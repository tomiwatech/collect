package question

import (
	"context"
	"errors"
	"fmt"
)

var (
	ErrorFetchingQuestion = errors.New("failed to fetch question by form id")
	ErrorCreatingQuestion = errors.New("failed to create a new question")
)

type Item struct {
	Question            string `json:"question"`
	QuestionType        string `json:"type"`
	Description         string `json:"description"`
	Keyword             string `json:"keyword"`
	ImageUrl            string `json:"imageUrl"`
	MaxCharacterAllowed int    `json:"maxCharacterAllowed"`
}

type Question struct {
	FormId       string `json:"formId"`
	QuestionItem []Item `json:"questionItem"`
}

type ItemDTO struct {
	ID                  string `bson:"_id"`
	Question            string `bson:"question"`
	FormId              string `bson:"formId"`
	QuestionType        string `bson:"type"`
	Description         string `bson:"description"`
	Keyword             string `bson:"keyword"`
	ImageUrl            string `bson:"imageUrl"`
	MaxCharacterAllowed int    `bson:"maxCharacterAllowed"`
}

func ToQuestionDTO(question Question) []ItemDTO {
	questionItem := question.QuestionItem
	itemDto := make([]ItemDTO, 0)
	for _, item := range questionItem {
		currentDto := ItemDTO{
			FormId:              question.FormId,
			Question:            item.Question,
			QuestionType:        item.QuestionType,
			Description:         item.Description,
			Keyword:             item.Keyword,
			ImageUrl:            item.ImageUrl,
			MaxCharacterAllowed: item.MaxCharacterAllowed,
		}
		itemDto = append(itemDto, currentDto)
	}
	return itemDto
}

type QuestionStore interface {
	GetQuestion(context.Context, string) (ItemDTO, error)
	GetQuestions(context.Context) ([]ItemDTO, error)
	GetQuestionsByForm(context.Context, string) ([]ItemDTO, error)
	CreateQuestion(context.Context, Question) (Question, error)
}

type QuestionStruct struct {
	QuestionStore QuestionStore
}

func NewQuestion(questionStore QuestionStore) *QuestionStruct {
	return &QuestionStruct{
		QuestionStore: questionStore,
	}
}

func (s *QuestionStruct) CreateQuestion(ctx context.Context, userQuestion Question) (Question, error) {
	fmt.Println("Creating a question")
	question, err := s.QuestionStore.CreateQuestion(ctx, userQuestion)
	if err != nil {
		fmt.Println(err)
		return Question{}, ErrorCreatingQuestion
	}

	return question, nil
}

func (s *QuestionStruct) GetQuestion(ctx context.Context, formId string) (ItemDTO, error) {
	fmt.Println("Getting a question")
	question, err := s.QuestionStore.GetQuestion(ctx, formId)
	if err != nil {
		fmt.Println(err)
		return ItemDTO{}, ErrorFetchingQuestion
	}
	return question, nil
}

func (s *QuestionStruct) GetQuestions(ctx context.Context) ([]ItemDTO, error) {
	fmt.Println("Getting a question")
	question, err := s.QuestionStore.GetQuestions(ctx)
	if err != nil {
		fmt.Println(err)
		return []ItemDTO{}, ErrorFetchingQuestion
	}
	return question, nil
}

func (s *QuestionStruct) GetQuestionsByForm(ctx context.Context, formId string) ([]ItemDTO, error) {
	fmt.Println("Getting questions by form")
	questions, err := s.QuestionStore.GetQuestionsByForm(ctx, formId)
	if err != nil {
		fmt.Println(err)
		return []ItemDTO{}, ErrorFetchingQuestion
	}
	return questions, nil
}
