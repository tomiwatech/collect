package rabbitmq

import (
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/tomiwatech/collect/internal/config"
)

var configInfo config.Config

func init() {
	configInfo.Read()
}

func Connect() *amqp.Connection {
	fmt.Println(configInfo.Server.Amqp)
	connection, err := amqp.Dial(configInfo.Server.Amqp)
	if err != nil {
		fmt.Println(err)
		panic(1)
	}
	fmt.Println("Successfully Connected to RabbitMQ Instance")
	return connection
}
