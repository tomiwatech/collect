package rabbitmq

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/tomiwatech/collect/internal/integration"
	"gitlab.com/tomiwatech/collect/internal/integration/helper"
)

func ConsumeMessage() {
	conn := Connect()
	ch, err := conn.Channel()
	if err != nil {
		fmt.Println(err)
	}
	defer func(ch *amqp.Channel) {
		err := ch.Close()
		if err != nil {

		}
	}(ch)

	messages, err := ch.Consume(
		"TestQueue",
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	forever := make(chan bool)
	go func() {
		for d := range messages {
			fmt.Printf("Recieved Message: %s\n", d.Body)
			var messageBody MessageBody
			err := json.Unmarshal(d.Body, &messageBody)
			if err != nil {
				fmt.Println(err)
				return
			}
			integrationData := integration.Data{
				Name:         "GoogleSheet",
				FormId:       messageBody.Submission.FormId,
				SubmissionId: messageBody.SubmissionId,
			}

			submissions := make([]string, 0)
			for _, item := range messageBody.Submission.SubmissionItem {
				submissions = append(submissions, item.Response)
			}
			helper.ProcessGoogleSheetIntegration(integrationData, submissions)
		}
	}()

	fmt.Println(" [*] - Waiting for messages")
	<-forever
}
