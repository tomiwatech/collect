package rabbitmq

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/tomiwatech/collect/internal/submission"
)

type MessageBody struct {
	Submission   submission.Submission `json:"submission"`
	SubmissionId string                `json:"submissionId"`
}

func PublishMessageToQueue(submission submission.Submission, submissionId string) {
	conn := Connect()
	ch, err := conn.Channel()
	if err != nil {
		fmt.Println(err)
	}
	defer func(ch *amqp.Channel) {
		err := ch.Close()
		if err != nil {

		}
	}(ch)

	queue, err := ch.QueueDeclare(
		"TestQueue",
		false,
		false,
		false,
		false,
		nil,
	)
	fmt.Println(queue)
	if err != nil {
		fmt.Println(err)
	}

	messageBody := MessageBody{Submission: submission, SubmissionId: submissionId}
	jsonSubmission, err := json.Marshal(messageBody)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = ch.Publish(
		"",
		"TestQueue",
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(jsonSubmission),
		},
	)

	if err != nil {
		fmt.Println(err)
	}
}
