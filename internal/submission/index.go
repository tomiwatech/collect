package submission

import (
	"context"
	"errors"
	"fmt"
)

var (
	ErrorFetchingSubmission = errors.New("failed to fetch submission by form id")
	ErrorCreatingSubmission = errors.New("failed to create a new submission")
)

type SubmissionItem struct {
	QuestionId string `json:"questionId"`
	Response   string `json:"response"`
}

type Submission struct {
	FormId         string           `json:"formId"`
	SubmissionItem []SubmissionItem `json:"submissionItem"`
}

type SubmissionDTO struct {
	ID         string `bson:"_id"`
	QuestionId string `bson:"questionId"`
	Response   string `bson:"response"`
}

type SubmissionData struct {
	FormId    string          `bson:"formId"`
	Responses []SubmissionDTO `bson:"responses"`
}

type SubmissionStore interface {
	GetSubmission(context.Context, string) (SubmissionData, error)
	GetSubmissions(context.Context) ([]SubmissionData, error)
	GetSubmissionsByForm(context.Context, string) ([]SubmissionData, error)
	CreateSubmission(context.Context, Submission) (SubmissionData, string, error)
}

type SubmissionStruct struct {
	SubmissionStore SubmissionStore
}

func ToSubmissionDTO(submission Submission) SubmissionData {
	submissions := submission.SubmissionItem
	newSubmissions := make([]SubmissionDTO, 0)
	for _, item := range submissions {
		currentSubmission := SubmissionDTO{
			ID:         "",
			QuestionId: item.QuestionId,
			Response:   item.Response,
		}
		newSubmissions = append(newSubmissions, currentSubmission)
	}
	return SubmissionData{
		FormId:    submission.FormId,
		Responses: newSubmissions,
	}
}

func NewSubmission(submissionStore SubmissionStore) *SubmissionStruct {
	return &SubmissionStruct{
		SubmissionStore: submissionStore,
	}
}

func (s *SubmissionStruct) CreateSubmission(ctx context.Context, userSubmission Submission) (SubmissionData, string, error) {
	fmt.Println("Creating a submission")

	submission, submissionId, err := s.SubmissionStore.CreateSubmission(ctx, userSubmission)
	if err != nil {
		fmt.Println(err)
		return SubmissionData{}, "", ErrorCreatingSubmission
	}

	return submission, submissionId, nil
}

func (s *SubmissionStruct) GetSubmission(ctx context.Context, formId string) (SubmissionData, error) {
	fmt.Println("Getting a submission")
	submission, err := s.SubmissionStore.GetSubmission(ctx, formId)
	if err != nil {
		fmt.Println(err)
		return SubmissionData{}, ErrorFetchingSubmission
	}
	return submission, nil
}

func (s *SubmissionStruct) GetSubmissions(ctx context.Context) ([]SubmissionData, error) {
	fmt.Println("Getting a submission")
	submissions, err := s.SubmissionStore.GetSubmissions(ctx)
	if err != nil {
		fmt.Println(err)
		return []SubmissionData{}, ErrorFetchingSubmission
	}
	return submissions, nil
}

func (s *SubmissionStruct) GetSubmissionsByForm(ctx context.Context, formId string) ([]SubmissionData, error) {
	fmt.Println("Getting submissions")
	submissions, err := s.SubmissionStore.GetSubmissionsByForm(ctx, formId)
	if err != nil {
		fmt.Println(err)
		return []SubmissionData{}, ErrorFetchingSubmission
	}
	return submissions, nil
}
