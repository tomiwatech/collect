// +build e2e

package tests

import (
	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFormEndpoint(t *testing.T) {
	t.Run("can create form", func(t *testing.T) {
		client := resty.New()
		resp, err := client.R().SetBody(
			`{"name": "Testing form"}`).Post("http://localhost:8888/api/forms")

		assert.NoError(t, err)
		assert.Equal(t, 201, resp.StatusCode())
	})

	t.Run("can get form", func(t *testing.T) {
		client := resty.New()
		resp, err := client.R().Get("http://localhost:8888/api/forms/Testing form")

		assert.NoError(t, err)
		assert.Equal(t, 200, resp.StatusCode())
	})
}
