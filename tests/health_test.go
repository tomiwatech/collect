// +build e2e

package tests

import (
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHealthCheckEndpoint(t *testing.T) {
	client := resty.New()
	response, err := client.R().Get("http://localhost:8888")
	fmt.Println(response.Body())
	assert.NoError(t, err)
	assert.Equal(t, 200, response.StatusCode())
}
