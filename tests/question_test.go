// +build e2e

package tests

import (
	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestQuestionEndpoint(t *testing.T) {
	t.Run("can create question", func(t *testing.T) {
		client := resty.New()
		resp, err := client.R().SetBody(
			`{"question": "Hello", "description": "Hello there", "type": "text", "formId": "ab00cd3f-9bbf-4584-9376-676876585a71", "keyword": "work", "imageUrl": "google.com", "maxCharacterAllowed": 10}`).Post("http://localhost:8888/api/questions")

		assert.NoError(t, err)
		assert.Equal(t, 201, resp.StatusCode())
	})

	t.Run("can get question", func(t *testing.T) {
		client := resty.New()
		resp, err := client.R().Get("http://localhost:8888/api/questions/ab00cd3f-9bbf-4584-9376-676876585a71")

		assert.NoError(t, err)
		assert.Equal(t, 200, resp.StatusCode())
	})
}
