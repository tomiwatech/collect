// +build e2e

package tests

import (
	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSubmissionEndpoint(t *testing.T) {
	t.Run("can create submission", func(t *testing.T) {
		client := resty.New()
		resp, err := client.R().SetBody(
			`{"question": "Hello", "formId": "ab00cd3f-9bbf-4584-9376-676876585a71", "response": "work"}`).Post("http://localhost:8888/api/submissions")

		assert.NoError(t, err)
		assert.Equal(t, 201, resp.StatusCode())
	})

	t.Run("can get submission by form id", func(t *testing.T) {
		client := resty.New()
		resp, err := client.R().Get("http://localhost:8888/api/submissions/ab00cd3f-9bbf-4584-9376-676876585a71")

		assert.NoError(t, err)
		assert.Equal(t, 200, resp.StatusCode())
	})
}
